<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prestamoVideo_model
 *
 * @author Usuario
 */
class Carrito_model extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
    }
    function addCarrito($data){
        $res=$this->db->insert("carrito", $data);
        $this->load->model("administracion/pelicula_model");
        $query=  $this->pelicula_model->getPeliculaById($data->PeliculaidPelicula);
        if($res==TRUE){
            foreach ($query->result() as $row) {
                return "El DVD ".$row->titulo." se ingreso al carrito exitosamente";
            }
            
        }else{
            return "Error al agregar DVD al carrito";
        }
    }
    function getTodoCarrito(){
        $consulta=  $this->db->get("carrito");
        return $consulta;
    }
    function getTodoCarritoById($idSocio){
        $consulta=  $this->db->get("carrito", array("SocioidCliente"=>$idSocio));
        return $consulta;
    }
    function borrarByIdSocio(){
        $resp="";
        $idSocio=$this->session->userdata("idSeleccion");
        $res=$this->db->delete("carrito", array("SocioidCliente"=>$idSocio));
        if($res==TRUE){
            $resp="Carrito borrado exitosamente";
        }else{
            $resp="Error al borrar carrito";
        }
        return $resp;
    }
    function borrarByIdPelicula($idPelicula){
        $resp="";
        $idSocio=$this->session->userdata("idSeleccion");
        $res=$this->db->delete("carrito", array("SocioidCliente"=>$idSocio, "PeliculaidPelicula"=>$idPelicula));
        
        if($res==TRUE){
            $resp="Carrito borrado exitosamente";
        }else{
            $resp="Error al borrar carrito";
        }
        return $resp;
    }
    function getNumDisp($idPelicula){
        $num=0;
        $query=  $this->db->get_where("pelicula", array("idPelicula"=>$idPelicula));
        foreach($query->result() as $row){
            $num=$row->numDVDDisponibles;
        }
        return $num;
    }
    function testCantidad($cantidad, $idPelicula){
        $resp=FALSE;
        if(($this->getNumDisp($idPelicula))<$cantidad){
            $resp=FALSE;
        }else{
            $resp=TRUE;
        }
        return $resp;
    }
    function updateCarrito($idSocio, $idPeli){
        $this->db->set('cantidad',"cantidad+1", FALSE);
        $this->db->where("PeliculaidPelicula",$idPeli);
        $this->db->where("SocioidCliente",$idSocio);
        $this->db->update('carrito');
    }
    function comprobarExistencia($idSocio, $idPeli){
        $query=$this->db->get_where("carrito", array("SocioidCliente"=>$idSocio,"PeliculaidPelicula"=>$idPeli));
        return $query->num_rows()>0 ?true:false;
    }
}
