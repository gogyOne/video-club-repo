<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Socio_model
 *
 * @author Usuario
 */
class Socio_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function addSocio($data) {
        $resp="";
        $res=$this->db->insert("socio", $data);
        if($res==true){
            $resp = "El socio de rut " . $data->getRut() . "-" . $data->getDv() . " ingresado exitosamente";
        }else{
            $resp = "Error al ingresar socio";
        }
        return $resp;
        /* $res=$this->db->insert("socio", array(
          "nombre" => $data["nombre"],
          "apellido" => $data["apellido"],
          "fechaNac" => $data["fechaNac"],
          "sexo" => $data["sexo"],
          "direccion" => $data["direccion"],
          "telefono" => $data["telefono"],
          "rut" => $data["rut"],
          "dv" => $data["dv"]
          ));
          return $res; */
    }

    function getTodoSocioByRut($rut = "", $dv = "") {
        $query = $this->db->get_where("socio", array("rut" => $rut, "dv" => $dv));
        if ($rut == "" && $dv == "") {
            
        } else {
            return $query->result();
        }
    }

    function getSocioById($idCliente) {
        $query = $this->db->get_where("socio", array("idCliente" => $idCliente));
        $this->load->library("POJO/socioPO", null, "socioPO");
        foreach ($query->result() as $row) {
            $objeto = new $this->socioPO($row->nombre, $row->apellido, $row->fechaNac, $row->sexo, $row->direccion, $row->telefono, $row->rut, $row->dv
            );
            return $objeto;
        }
    }
    

}

?>
