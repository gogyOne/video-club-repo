<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pelicula
 *
 * @author Usuario
 */
class Pelicula_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function addPelicula($objeto) {
        $resp = "";
        $res = $this->db->insert("pelicula", $objeto);
        if ($res == TRUE) {
            $resp = "La pelicula " . $objeto->getTitulo() . " ingresado exitosamente";
        } else {
            $resp = "Error al ingresar la pelicula";
        }
        return $resp;
    }

    function getTodoPelicula() {
        $query = $this->db->get("pelicula");
        return $query;
    }

    function getPeliculaById($id) {
        $query = $this->db->get_where("pelicula", array("idPelicula" => $id));
        return $query;
    }

    function updatePeliculaById($cantidad, $idPelicula) {
        $mensaje = "";
        $query = $this->getPeliculaById($idPelicula);
        $res = "";
        foreach ($query->result() as $row) {
            $nuevaDisp = (int) ($row->numDVDDisponibles) - (int) ($cantidad);
            $res = $this->db->update("pelicula", array("numDVDDisponibles" => $nuevaDisp, "isDisponible" => ($nuevaDisp == 0 ? 0 : 1)), array("idPelicula" => $idPelicula));
            break;
        }
        if ($res == true) {
            $mensaje = "Numero de DVD disponibles a sido actualizado con exito";
        } else {
            $mensaje = "Error al actualizar la pelicula";
        }
        return $mensaje;
    }

}
