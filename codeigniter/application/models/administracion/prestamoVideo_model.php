<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prestamoVideo_model
 *
 * @author Usuario
 */
class PrestamoVideo_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("administracion/carrito_model");
        $this->load->model("administracion/pelicula_model");
        $this->load->model("administracion/socio_model");
        $this->load->library("POJO/prestamovideoPO");
    }

    function addPrestamo($data) {
        $resp = "";
        $res = $this->db->insert("prestamovideo", $data);
        $titulo1 = "";
        $queryPelicula = $this->pelicula_model->getPeliculaById($data->PeliculaidPelicula);
        foreach ($queryPelicula->result() as $row1) {
            $titulo1 = $row1->titulo;
            break;
        }
        if ($res == TRUE) {
            $resp = "Prestamo de DVD " . $titulo1 . " ingresado con exito";
        } else {
            $resp = "Error al ingresar prestamo de DVD " . $titulo1;
        }
        return $resp;
    }

    function getTodoPrestamo() {
        $consulta = $this->db->get("prestamovideo");
        return $consulta->result();
    }

    function getPrestamoVideoByIdSocioEstado($idSocio, $estado) {
        $query = $this->db->get_where("prestamovideo", array("estado" => $estado, "SocioidCliente" => $idSocio));
        return $query;
    }
    function getPrestamoById($id){
        $query=$this->db->get_where("prestamovideo", array("idPrestamo"=>$id));
        return $query;
    }
    function updateEstado($idPrestamo, $nuevoEstado) {
        $resp = "";
        $estadoAux="";
        if($nuevoEstado==TRUE){
            $estadoAux="Pagado";
        }else{
            $estadoAux="Pendiente";
        }
        $data = array("estado" => $estadoAux);
        $res = $this->db->update("prestamovideo", $data, array("idPrestamo" => $idPrestamo));
        if($res==true){
            $resp="Estado de ID Prestamo ".$idPrestamo." actualizado con exito a ".$estadoAux;
        }else{
            $resp="Error al actualizar estado de prestamo";
        }
        return $resp;
    }

}
