<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body>
    <center>
        <?php echo validation_errors(); ?>
        <?php
        echo "<table>";
        echo form_open("carrito/recibirDatosVistaCarrito",array("class"=>"elegant-aero"));
        echo "<tr>";
        echo "<td>Quitar</td>"
        . "<td>Titulo</td>"
        . "<td>Cantidad</td>"
        . "<td>Precio</td>";
        echo "</tr>";
        foreach ($salida->result() as $row) {
            echo "<tr>";
            echo "<td>" . anchor("carrito/borrarById/" . $row->PeliculaidPelicula, "X") . "</td>";
            $query2 = $this->db->get_where('pelicula', array('idPelicula' => (int)$row->PeliculaidPelicula));
            foreach ($query2->result() as $valor) {
                
                echo "<td>" . $valor->titulo . "" . form_hidden("listPeliculas[]", $row->PeliculaidPelicula) . "</td>";
                echo "<td>" . form_input("listCantidad[]", $row->cantidad) . "</td>";
                echo "<td>" . $valor->precio . "" . form_hidden("listPrecio[]", $valor->precio) . "</td>";
                echo "</tr>";
            }
        }
        echo "</table>";
        echo form_submit(array("name"=>"siguiente", "class"=>"button"), "Siguiente");
        echo form_close();
        ?>
    </center>
</body>
</html>

