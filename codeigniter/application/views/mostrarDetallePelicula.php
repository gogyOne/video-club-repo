<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body class="elegant-aero">
        <center>
        <?php
        foreach ($salida->result() as $row) {
            $this->table->add_row("Titulo", $row->titulo);
            $this->table->add_row("Tema",$row->tema);
            $this->table->add_row("Año de Producción",$row->anhoProduccion);
            $this->table->add_row("Actor Principal",$row->actorPrincipal);
            $this->table->add_row("Nombre Productora",$row->nombreProductora);
            $this->table->add_row("DVD Disponibles",$row->numDVDDisponibles);
            $this->table->add_row("Disponibilidad",($row->isDisponible=="1"||$row->isDisponible==1?'En stock':'Agotado'));
            $this->table->add_row("Precio Unitario",$row->precio);
            echo anchor("carrito/addCarrito/".$row->idPelicula,"Prestar");
        }
        echo $this->table->generate();
        /*
        echo "<table>";
        foreach ($salida->result() as $row) {
            echo "<tr>";
            echo "<td>Titulo</td>";
            echo "<td>" . $row->titulo . "</td>";
            echo "</tr><tr>";
            echo "<td>Tema</td>";
            echo "<td>" . $row->tema . "</td>";
            echo "</tr><tr>";
            echo "<td>Año de Producción</td>";
            echo "<td>" . $row->anhoProduccion . "</td>";
            echo "</tr><tr>";
            echo "<td>Actor Principal</td>";
            echo "<td>" . $row->actorPrincipal . "</td>";
            echo "</tr><tr>";
            echo "<td>Nombre Productora</td>";
            echo "<td>" . $row->nombreProductora . "</td>";
            echo "</tr><tr>";
            echo "<td>DVD Disponibles</td>";
            echo "<td>" . $row->numDVDDisponibles . "</td>";
            echo "</tr><tr>";
            echo "<td>Disponibilidad</td>";
            echo "<td>" . ($row->isDisponible=="1"||$row->isDisponible==1?'En stock':'Agotado'). "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Precio Unitario</td>";
            echo "<td>" . $row->precio . "</td>";
            echo "</tr>";
            echo anchor("carrito/addCarrito/".$row->idPelicula,"Prestar");
        }
        echo "</table>";
        */
        ?>
    </center>
    </body>
</html>
