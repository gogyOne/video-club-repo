<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body>
        <?php
        echo form_open("prestamovideo/actualizarEstado", array("class"=>"elegant-aero"));
        echo "<table>";
        echo "<tr>";
        echo "<td>ID Prestamo</td>";
        echo "<td>Fecha Prestamo</td>"
        . "<td>Nombre</td>"
        . "<td>Apellido</td>"
        . "<td>Titulo de Pelicula</td>"
        . "<td>Estado</td>"
        . "<td>Pago Total</td>"
        . "<td>Estado Nuevo</td>";
        echo "</tr>";
        foreach ($salida->result() as $row) {
            echo "<tr>";
            echo "<td>". $row->idPrestamo."</td>";
            echo "<td>". $row->fechaPrestamo."</td>";
            $querySocio = $this->db->get_where('socio', array('idCliente' => $row->SocioidCliente));
            foreach ($querySocio->result() as $valor1) {
                echo "<td>" .$valor1->nombre."</td>";
                echo "<td>" .$valor1->apellido."</td>";
            }
            $queryPelicula = $this->db->get_where('pelicula', array('idPelicula' => $row->PeliculaidPelicula));
            foreach ($queryPelicula->result() as $valor2) {
                echo "<td>" .$valor2->titulo."</td>";
            }
            echo "<td>" .$row->estado."</td>";
            echo "<td>" .$row->precioTotal."</td>";
            echo "<td>" .form_checkbox("estadoNuevo[]", TRUE)."<br>".  form_hidden("listIdPrestamo[]", $row->idPrestamo)."</td>";
            echo "</tr>";
        }
        echo "</table>"; 
        echo form_submit(array("name"=>"actualizarTodo","class"=>"button"), "Actualizar Todo");
        echo form_close();
        ?>
    </body>
</html>
