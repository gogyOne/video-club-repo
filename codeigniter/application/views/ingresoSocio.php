<html>
    <head>
        <title>Ingreso Socio</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
        <script>
            $(function() {
                $("#datepicker").datepicker();
            });
        </script>
    </head>
    <body>
    <center>
        <?php echo validation_errors(); ?>
        <?php
        echo form_open("socio/recibirDatos", array("class" => "elegant-aero"));
        echo "<table>";
        foreach ($datos as $key => $value) {
            echo "<tr>";
            if ($key == "sexo") {
                echo "<td>" . $value . "</td><td>M" . form_radio($key, "M") . "</td><td>F" . form_radio($key, "F") . "</td>";
            } else if ($key == "fechaNac") {
                $calendario = array('name' => 'fechaNac', 'placeholder' => '01/01/1996', 'type' => 'text', 'id' => 'datepicker');
                echo "<td>" . $value . "</td><td>" . form_input($calendario) . "</td>";
            } else {
                echo "<td>" . $value . "</td><td>" . form_input($key) . "</td>";
            }
            echo "</tr><br>";
        }
        echo "<tr><td>";
        echo form_submit(array("name" => "agregar", "class" => "button"), "agregar");
        echo "</td></tr>";
        echo form_close();
        echo "</table>";
        ?>
    </center>
</body>
</html>
