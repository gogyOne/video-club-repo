<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body class="elegant-aero">
        <?php
        echo "<table>";
        echo "<tr>";
        echo "<td>Id Prestamo</td>"
        ."<td>Fecha Prestamo</td>"
        . "<td>Nombre</td>"
        . "<td>Apellido</td>"
        . "<td>Titulo de Pelicula</td>"
        . "<td>Estado</td>"
        . "<td>Pago Total</td>";
        echo "</tr>";
        foreach ($salida->result() as $row) {
            echo "<tr>";
            echo "<td>". $row->idPrestamo."</td>";
            echo "<td>". $row->fechaPrestamo."</td>";
            $querySocio=$this->db->get_where("socio", array("idCliente"=>$row->SocioidCliente));
            foreach ($querySocio->result() as $value) {
                echo "<td>" .$value->nombre."</td>";
                echo "<td>" .$value->apellido."</td>";
                break;
            }
            $queryPelicula=$this->db->get_where("pelicula", array("idPelicula"=>$row->PeliculaidPelicula));
            foreach ($queryPelicula->result() as $value) {
                echo "<td>" .$value->titulo."</td>";
                break;
            }
            echo "<td>" .$row->estado."</td>";
            echo "<td>" .$row->precioTotal."</td>";
            echo "</tr>";
        }
        echo "</table>"; 
        ?>
    </body>
</html>
