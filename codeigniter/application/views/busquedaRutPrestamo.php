<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body>
        <?php
        echo form_open("prestamovideo/busquedaRutPrestamo", array("class"=>"elegant-aero"));
        echo "Rut : ".form_input(array("name" => "rut", "required title" => "de 7 a 8 caracteres", "pattern"=>".{7,8}"))."<br>";
        echo "DV : ".form_input(array("name" => "dv", "required title" => "1 minimo", "pattern"=>".{1,1}"))."<br>";
        echo form_submit(array("name"=>"buscar","class"=>"button"), "Buscar Socio");
        echo form_close();
        if(isset($_POST['buscar'])){
            $rut=$_POST["rut"];
            $dv=$_POST["dv"];
            redirect("socio/getTodoSocioByRutPrestamo/$rut/$dv",'refresh');
        }
        ?>
    </body>
</html>
