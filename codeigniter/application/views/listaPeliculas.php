<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            html{
                padding: 0px;
                margin: 0px;
            }
            body{
                background-color: #ccc;
                padding: 0px;
                margin: 0px;
            }
            header{
                width: 100%;
                height: 60px;
                background-color: black;
                color:white;
            }
            header h1{
                margin-left: 10%;
                width: 30%;
                display: inline-block;
                vertical-align: top;
            }
            header a{
                width: 50px;
                margin-left: 50%;
                display: inline-block;
                vertical-align: top;
            }
            header a img{
                width: 100%;
            }
            section{
                width: 80%;
                min-height: 500px;
                border: 1px solid #DDD;
                padding: 2%;
                margin:0 auto;
                margin-left: 10%;
                margin-top: 50px;
            }
            .producto{
                width: 23%;
                height: 220px;
                background-color: #fafafa;
                border:1px solid gray;
                display: inline-block;
                vertical-align: top;
                margin-left: 1%;
                margin-top: 1%;
            }
        </style>
    </head>
    <body>

        <?php
        foreach ($salida->result() as $row) {
            echo "<div class='producto'>";
            echo "<center>";
            echo "<span>" . $row->titulo . "</span><br>";
            echo anchor("pelicula/mostrarPeliculaById/" . $row->idPelicula, "ver");
            echo "</center>";
            echo "</div>";
        }
        echo anchor("carrito/verCarrito", "Ver Carrito");
        ?>
        
    </body>
</html>
