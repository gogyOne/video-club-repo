<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" href="<?php echo base_url() ?>css/miestilo.css" rel="stylesheet"/>
    </head>
    <body>
    <center>
        <?php
            echo form_open("pelicula/addPelicula",array("class"=>"elegant-aero"));
            echo form_submit(array("name"=>"agregarPelicula","class"=>"button"), "Agregar Pelicula");
            echo form_close();
            echo form_open("socio/addSocio",array("class"=>"elegant-aero"));
            echo form_submit(array("name"=>"agregarSocio", "class"=>"button"), "Agregar Socio");
            echo form_close();
            echo form_open("prestamovideo/busquedaRut",array("class"=>"elegant-aero"));
            echo form_submit(array("name"=>"prestarPelicula","class"=>"button"), "Prestar Pelicula");
            echo form_close();
            echo form_open("prestamovideo/busquedaRutPrestamo",array("class"=>"elegant-aero"));
            echo form_submit(array("name"=>"administracionPago","class"=>"button"), "Administrar Pago Video de Socio");
            echo form_close();
        ?>
    </center>
    </body>
</html>
