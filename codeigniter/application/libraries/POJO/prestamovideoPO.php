<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prestamovideo
 *
 * @author Usuario
 */
class PrestamovideoPO {

    var $fechaPrestamo;
    var $SocioidCliente;
    var $PeliculaidPelicula;
    var $estado;
    var $precioTotal;
    /**
     * 
     * @return type
     */
    public function getEstado() {
        return $this->estado;
    }
    /**
     * 
     * @return type
     */
    public function getPrecioTotal() {
        return $this->precioTotal;
    }
    /**
     * 
     * @param type $estado
     */
    public function setEstado($estado) {
        $this->estado = $estado;
    }
    /**
     * 
     * @param type $precioTotal
     */
    public function setPrecioTotal($precioTotal) {
        $this->precioTotal = $precioTotal;
    }
    /**
     * 
     * @param type $fechaPrestamo
     * @param type $SocioidCliente
     * @param type $PeliculaidPelicula
     * @param type $estado
     * @param type $precioTotal
     */
    function PrestamovideoPO($fechaPrestamo = "", $SocioidCliente = 0, $PeliculaidPelicula = 0, $estado = "", $precioTotal = 0) {
        $this->fechaPrestamo = $fechaPrestamo;
        $this->SocioidCliente = $SocioidCliente;
        $this->PeliculaidPelicula = $PeliculaidPelicula;
        $this->estado = $estado;
        $this->precioTotal = $precioTotal;
    }
    /**
     * 
     * @return type
     */
    public function getFechaPrestamo() {
        return $this->fechaPrestamo;
    }
    /**
     * 
     * @return type
     */
    public function getSocioidCliente() {
        return $this->SocioidCliente;
    }
    /**
     * 
     * @return type
     */
    public function getPeliculaidPelicula() {
        return $this->PeliculaidPelicula;
    }
    /**
     * 
     * @param type $fechaPrestamo
     */
    public function setFechaPrestamo($fechaPrestamo) {
        $this->fechaPrestamo = $fechaPrestamo;
    }
    /**
     * 
     * @param type $SocioidCliente
     */
    public function setSocioidCliente($SocioidCliente) {
        $this->SocioidCliente = $SocioidCliente;
    }
    /**
     * 
     * @param type $PeliculaidPelicula
     */
    public function setPeliculaidPelicula($PeliculaidPelicula) {
        $this->PeliculaidPelicula = $PeliculaidPelicula;
    }

}
