<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pelicula
 *
 * @author Usuario
 */
class PeliculaPO {
    
    var $titulo;
    var $tema;
    var $anhoProduccion;
    var $actorPrincipal;
    var $nombreProductora;
    var $numDVDDisponibles;
    var $isDisponible;
    var $precio;
    /**
     * 
     * @return type
     */
    public function getPrecio() {
        return $this->precio;
    }
    /**
     * 
     * @param type $precio
     */
    public function setPrecio($precio) {
        $this->precio = $precio;
    }
    /**
     * 
     * @return type
     */
    public function getTitulo() {
        return $this->titulo;
    }
    /**
     * 
     * @return type
     */
    public function getTema() {
        return $this->tema;
    }
    /**
     * 
     * @return type
     */
    public function getAnhoProduccion() {
        return $this->anhoProduccion;
    }
    /**
     * 
     * @return type
     */
    public function getActorPrincipal() {
        return $this->actorPrincipal;
    }
    /**
     * 
     * @return type
     */
    public function getNombreProductora() {
        return $this->nombreProductora;
    }
    /**
     * 
     * @return type
     */
    public function getNumDVDDisponibles() {
        return $this->numDVDDisponibles;
    }
    /**
     * 
     * @return type
     */
    public function getIsDisponible() {
        return $this->isDisponible;
    }
    /**
     * 
     * @param type $titulo
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }
    /**
     * 
     * @param type $tema
     */
    public function setTema($tema) {
        $this->tema = $tema;
    }
    /**
     * 
     * @param type $anhoProduccion
     */
    public function setAnhoProduccion($anhoProduccion) {
        $this->anhoProduccion = $anhoProduccion;
    }
    /**
     * 
     * @param type $actorPrincipal
     */
    public function setActorPrincipal($actorPrincipal) {
        $this->actorPrincipal = $actorPrincipal;
    }
    /**
     * 
     * @param type $nombreProductora
     */
    public function setNombreProductora($nombreProductora) {
        $this->nombreProductora = $nombreProductora;
    }
    /**
     * 
     * @param type $numDVDDisponibles
     */
    public function setNumDVDDisponibles($numDVDDisponibles) {
        $this->numDVDDisponibles = $numDVDDisponibles;
    }
    /**
     * 
     * @param type $isDisponible
     */
    public function setIsDisponible($isDisponible) {
        $this->isDisponible = $isDisponible;
    }
    /**
     * 
     * @param type $titulo
     * @param type $tema
     * @param type $anhoProduccion
     * @param type $actorPrincipal
     * @param type $nombreProductora
     * @param type $numDVDDisponibles
     * @param type $isDisponible
     * @param type $precio
     */
    function PeliculaPO($titulo = "", $tema = "", $anhoProduccion = 0, $actorPrincipal = "", $nombreProductora = "", $numDVDDisponibles = 0, $isDisponible = 0, $precio = 0) {
        $this->titulo = $titulo;
        $this->tema = $tema;
        $this->anhoProduccion = $anhoProduccion;
        $this->actorPrincipal = $actorPrincipal;
        $this->nombreProductora = $nombreProductora;
        $this->numDVDDisponibles = $numDVDDisponibles;
        $this->isDisponible = $isDisponible;
        $this->precio = $precio;
    }

}
