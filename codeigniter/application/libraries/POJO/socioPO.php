<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Socio
 *
 * @author Usuario
 */
class SocioPO {
    var $nombre;
    var $apellido;
    var $fechaNac;
    var $sexo;
    var $direccion;
    var $telefono;
    var $rut;
    var $dv;
    /**
     * 
     * @param type $nombre
     * @param type $apellido
     * @param type $fechaNac
     * @param type $sexo
     * @param type $direccion
     * @param type $telefono
     * @param type $rut
     * @param type $dv
     */
    function SocioPO($nombre="", $apellido="", $fechaNac="", $sexo="", $direccion="", $telefono="", $rut="", $dv="") {
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->fechaNac = $fechaNac;
        $this->sexo = $sexo;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->rut = $rut;
        $this->dv = $dv;
    }
    /**
     * 
     * @return type
     */
    public function getNombre() {
        return $this->nombre;
    }
    /**
     * 
     * @return type
     */
    public function getApellido() {
        return $this->apellido;
    }
    /**
     * 
     * @return type
     */
    public function getFechaNac() {
        return $this->fechaNac;
    }
    /**
     * 
     * @return type
     */
    public function getSexo() {
        return $this->sexo;
    }
    /**
     * 
     * @return type
     */
    public function getDireccion() {
        return $this->direccion;
    }
    /**
     * 
     * @return type
     */
    public function getTelefono() {
        return $this->telefono;
    }
    /**
     * 
     * @return type
     */
    public function getRut() {
        return $this->rut;
    }
    /**
     * 
     * @return type
     */
    public function getDv() {
        return $this->dv;
    }
    /**
     * 
     * @param type $nombre
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    /**
     * 
     * @param type $apellido
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }
    /**
     * 
     * @param type $fechaNac
     */
    public function setFechaNac($fechaNac) {
        $this->fechaNac = $fechaNac;
    }
    /**
     * 
     * @param type $sexo
     */
    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }
    /**
     * 
     * @param type $direccion
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }
    /**
     * 
     * @param type $telefono
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }
    /**
     * 
     * @param type $rut
     */
    public function setRut($rut) {
        $this->rut = $rut;
    }
    /**
     * 
     * @param type $dv
     */
    public function setDv($dv) {
        $this->dv = $dv;
    }
}
?>
