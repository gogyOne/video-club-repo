<?php

require_once(APPPATH . 'libraries/POJO/socioPO.php');
require_once(APPPATH . 'libraries/POJO/peliculaPO.php');
/**
 * 
 * @author Usuario
 *
 */
class CarritoPO {
    var $fechaPrestamo;
    var $SocioidCliente;
    var $PeliculaidPelicula;
    var $cantidad;
	/**
	 * 
	 * @param number $SocioidCliente
	 * @param number $PeliculaidPelicula
	 * @param string $fechaPrestamo
	 * @param number $cantidad
	 */
    function CarritoPO($SocioidCliente = 0, $PeliculaidPelicula = 0, $fechaPrestamo = "", $cantidad = 1) {
        $this->fechaPrestamo = $fechaPrestamo;
        $this->SocioidCliente = $SocioidCliente;
        $this->PeliculaidPelicula = $PeliculaidPelicula;
        $this->cantidad = $cantidad;
    }
    /**
     * 
     * @return type
     */
    public function getCantidad() {
        return $this->cantidad;
    }
    /**
     * 
     * @param type $cantidad
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    /**
     * 
     * @return type
     */
    public function getFechaPrestamo() {
        return $this->fechaPrestamo;
    }
    /**
     * 
     * @return type
     */
    public function getSocioidCliente() {
        return $this->SocioidCliente;
    }
    /**
     * 
     * @return type
     */
    public function getPeliculaidPelicula() {
        return $this->PeliculaidPelicula;
    }
    /**
     * 
     * @param type $fechaPrestamo
     */
    public function setFechaPrestamo($fechaPrestamo) {
        $this->fechaPrestamo = $fechaPrestamo;
    }
    /**
     * 
     * @param type $SocioidCliente
     */
    public function setSocioidCliente($SocioidCliente) {
        $this->SocioidCliente = $SocioidCliente;
    }
    /**
     * 
     * @param type $PeliculaidPelicula
     */
    public function setPeliculaidPelicula($PeliculaidPelicula) {
        $this->PeliculaidPelicula = $PeliculaidPelicula;
    }

}
