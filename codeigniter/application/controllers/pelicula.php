<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pelicula
 *
 * @author Usuario
 */
class Pelicula extends CI_Controller {
    /**
     * 
     */
    function __construct() {
        parent::__construct();
        //cagando model
        $this->load->model("administracion/pelicula_model");
        //cargando helper
        $this->load->helper("form_helper");
        $this->load->helper("url");
        //cagando libreria para table
        $this->load->library("table");
        
    }
    /**
     * 
     */
    function addPelicula() {
        //generando formulario
        $datos["datos"] = array(
            "titulo" => "Titulo",
            "tema" => "Tema",
            "anhoProduccion" => "Año Produccion",
            "actorPrincipal" => "Actor Principal",
            "nombreProductora" => "Nombre Productora",
            "numDVDDisponibles" => "Num DVD Disponibles",
            "precio" => "Precio Unitario");
        /*$this->load->library('form_validation');
        $this->form_validation->set_rules('titulo', 'Titulo', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ingresoPelicula',$datos);
        } else {
            $this->load->view('formsuccess');
        }*/
        
        
        $this->load->view('ingresoPelicula', $datos);
    }
    /**
     * recibir datos del formulario pelicula
     */
    function recibirDatos() {
        $this->load->library("POJO/peliculaPO", null, "peliculaPO");
        //obteniendo num dvd disponibles del formulario
        $numDisp = CI_Controller::get_instance()->input->post("numDVDDisponibles");
        $estaDisp = 0;
        // 1 para disponible, 0 si el producto esta agotado
        if ($numDisp > 0) {
            $estaDisp = 1;
        }
        //creando objeto pelicula
        $objeto = new $this->peliculaPO(
                CI_Controller::get_instance()->input->post("titulo"), CI_Controller::get_instance()->input->post("tema"), CI_Controller::get_instance()->input->post("anhoProduccion"), CI_Controller::get_instance()->input->post("actorPrincipal"), CI_Controller::get_instance()->input->post("nombreProductora"), $numDisp, $estaDisp, CI_Controller::get_instance()->input->post("precio")
        );
        
        //validacion
        $datos["datos"] = array(
            "titulo" => "Titulo",
            "tema" => "Tema",
            "anhoProduccion" => "Año Produccion",
            "actorPrincipal" => "Actor Principal",
            "nombreProductora" => "Nombre Productora",
            "numDVDDisponibles" => "Num DVD Disponibles",
            "precio" => "Precio Unitario");
        $this->load->library('form_validation');
        //creando reglas de validacion
        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|max_length[40]');
        $this->form_validation->set_rules('tema', 'Tema', 'trim|required|max_length[40]');
        $this->form_validation->set_rules('anhoProduccion', 'Año Produccion', 'trim|required|integer|max_length[4]');
        $this->form_validation->set_rules('actorPrincipal', 'Actor Principal', 'trim|required|max_length[254]');
        $this->form_validation->set_rules('nombreProductora', 'Nombre Productora', 'trim|required|max_length[40]');
        $this->form_validation->set_rules('numDVDDisponibles', 'Num DVD Disponibles', 'trim|required|integer|max_length[10]');
        $this->form_validation->set_rules('precio', 'Precio', 'trim|required|numeric|max_length[11]');
        //donde colocar el mensaje de error
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE) {
            //vuelve al actual
            $this->load->view('ingresoPelicula',$datos);
        } else {
            //muestra un mensaje exitoso
            $res["salida"] = $this->pelicula_model->addPelicula($objeto);
            $this->load->view("salida", $res);
        }
        //$this->load->view("salida", $res);
    }
    /**
     * 
     */
    function getTodoPelicula() {
        
    }
    /**
     * 
     */
    public function index() {
        
    }
    /**
     * retornar a la vista que muestra detalles de la pelicula
     * @param type $id
     */
    public function mostrarPeliculaById($id) {
        $this->load->helper("url");
        $query["salida"] = $this->pelicula_model->getPeliculaById($id);
        $this->load->view("mostrarDetallePelicula", $query);
    }

}