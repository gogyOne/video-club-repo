<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{   
            $this->load->helper("url");
            $this->load->library("session");
            $this->session->set_userdata("usuario","hola mundo");
            $this->session->set_userdata("usuario","hola chao");
            $data["data"]=$this->session->userdata("usuario");
            $this->load->view('listaPeliculas',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */