<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prestamovideo
 *
 * @author Usuario
 */
class Carrito extends CI_Controller {
    /**
     * 
     */
    function __construct() {
        parent::__construct();
        //cargar models
        $this->load->model("administracion/socio_model");
        $this->load->model("administracion/pelicula_model");
        $this->load->model("administracion/carrito_model");
        //cargar helper
        $this->load->helper("form");
        $this->load->helper("url");
        //cargar librerias
        $this->load->library("POJO/peliculaPO", null, "peliculaPO");
        $this->load->library("POJO/carritoPO", null, "carritoPO");
        $this->load->library("session");
        $this->load->library("validaciones/validacion", null, "validacion");
    }
    /**
     * 
     */
    function index() {
        
    }
    /**
     * 
     * @param type $idPeli
     */
    function addCarrito($idPeli) {
        //crea objeto carrito PO
        $idSocio = $this->session->userdata("idSeleccion");
        $fechaPrestamo1 = $this->validacion->getFechaActual();
        $objetoCarrito = new $this->carritoPO($idSocio, $idPeli, $fechaPrestamo1, 1);
        //si no existe en el carrito, la crear el registro en carrito. En caso de que exista, se actualiza el registro
        if($this->carrito_model->comprobarExistencia($idSocio, $idPeli)==false){
            //$data["salida"]="entro aca";
            
            //$this->load->view("salida",$data);
            $this->carrito_model->addCarrito($objetoCarrito);
        }else{
            //$data["salida"]="entro aqui";
            //$this->load->view("salida",$data);
            $this->carrito_model->updateCarrito($idSocio, $idPeli);
        }
        //ir la lista de peliculas
        $query["salida"] = $this->pelicula_model->getPeliculaById($idPeli);
        $this->load->view("listaPeliculas", $query);
    }
    /**
     * recibir datos vista carrito
     */
    function recibirDatosVistaCarrito() {
        
        $listCantidad = $this->input->post("listCantidad");
        $listPeliculas = $this->input->post("listPeliculas");
        $listPrecio = $this->input->post("listPrecio");
        //validando para cantidad 0
        $out1 = "";
        $this->load->library('form_validation');
        //for($cont=0;$cont<sizeof(""))
        $this->form_validation->set_rules('listCantidad[]', 'cantidad', 'trim|required|is_natural_no_zero|max_length[11]');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        //comprobar cantidad con disponibilidad
        //si cantidad es mayor al numero de dvd disponibles, entonces concatena el error
        $out1 = "";
        for ($cont = 0; $cont < sizeof($listCantidad); $cont++) {
            if ($this->comprobarCantidad($listPeliculas[$cont], $listCantidad[$cont]) == FALSE) {
                $query = $this->pelicula_model->getPeliculaById($listPeliculas[$cont]);
                foreach ($query->result() as $value) {
                    $out1.="Error, la cantidad ingresada de la pelicula " . $value->titulo . " no valido<br>";
                    break;
                }
            }
        }
        //calculo de precio total
        $precioTotal = 0;
        for ($cont = 0; $cont < sizeof($listCantidad); $cont++) {
            $precioTotal+=(int) ($listCantidad[$cont]) * (int) ($listPrecio[$cont]);
        }
        //guardan variables en variables de sesion
        $this->session->set_userdata("listCantidad", $listCantidad);
        $this->session->set_userdata("listIDPeli", $listPeliculas);
        $this->session->set_userdata("precioTotal", $precioTotal);
        if ($out1 != "") {
            //si hay error
            $out2["salida"] = $out1;
            $this->load->view("salida", $out2);
        } else {
            //si el formulario contiene errores
            if ($this->form_validation->run() == FALSE) {
                //error, devuelve al actual
                $idSocio = $this->session->userdata("idSeleccion");
                $query["salida"] = $this->carrito_model->getTodoCarritoById($idSocio);
                $this->load->view('vistaCarrito', $query);
            } else {
                //si todo fue exitoso
                $this->load->view("estadoCarrito");
            }
        }
    }
    /**
     * 
     * @param type $idPelicula
     * @param type $cantidad
     * @return boolean
     */
    //comprueba cantidad con disponibilidad
    function comprobarCantidad($idPelicula, $cantidad) {
        $resp = $this->carrito_model->testCantidad($cantidad, $idPelicula);
        if ($resp == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    /**
     * 
     * @param type $PeliculaidPelicula
     */
    //borra registro de carrito by id pelicula
    function borrarById($PeliculaidPelicula) {
        $this->carrito_model->borrarByIdPelicula($PeliculaidPelicula);
        $idSocio = $this->session->userdata("idSeleccion");
        $query["salida"] = $this->carrito_model->getTodoCarritoById($idSocio);
        $this->load->view("vistaCarrito", $query);
    }
    /**
     * 
     */
    function verCarrito() {
        $idSocio = $this->session->userdata("idSeleccion");
        $query["salida"] = $this->carrito_model->getTodoCarritoById($idSocio);
        $this->load->view("vistaCarrito", $query);
    }

}
