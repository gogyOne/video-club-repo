<!doctype html>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 */
class Socio extends CI_Controller {
    /**
     * 
     */
    function __construct() {
        parent::__construct();
        //cagando model
        $this->load->model("administracion/socio_model");
        $this->load->model("administracion/pelicula_model");
        //cagando helper
        $this->load->helper("form_helper");
        $this->load->helper("url");
        //cargando librerias
        $this->load->library("session");
        $this->load->library("table");
    }
    /**
     * 
     * @param type $rut
     * @param type $dv
     */
    function getTodoSocioByRut($rut = "", $dv = "") {
        $data["salida"] = $this->socio_model->getTodoSocioByRut($rut, $dv);

        $this->load->view("eleccionSocio", $data);
    }
    /**
     * 
     * @param type $rut
     * @param type $dv
     */
    function getTodoSocioByRutPrestamo($rut = "", $dv = "") {
        $data["salida"] = $this->socio_model->getTodoSocioByRut($rut, $dv);
        $this->load->view("eleccionSocioPrestamo", $data);
    }
    /**
     * 
     */
    function addSocio() {
        //creando formulario
        $this->load->helper("form_helper");
        $datos["datos"] = array(
            "nombre" => "Nombre",
            "apellido" => "Apellido",
            "fechaNac" => "Fecha de Nacimiento",
            "sexo" => "Sexo",
            "direccion" => "Direccion",
            "telefono" => "Telefono",
            "rut" => "Rut",
            "dv" => "DV");
        $this->load->view('ingresoSocio', $datos);
    }
    /**
     * 
     */
    function recibirDatos() {
        $datos["datos"] = array(
            "nombre" => "Nombre",
            "apellido" => "Apellido",
            "fechaNac" => "Fecha de Nacimiento",
            "sexo" => "Sexo",
            "direccion" => "Direccion",
            "telefono" => "Telefono",
            "rut" => "Rut",
            "dv" => "DV");
        $this->load->library("POJO/socioPO", null, "socioPO");
        //creando objeto socioPO
        $objeto = new $this->socioPO(CI_Controller::get_instance()->input->post("nombre"), CI_Controller::get_instance()->input->post("apellido"), CI_Controller::get_instance()->input->post("fechaNac"), CI_Controller::get_instance()->input->post("sexo"), CI_Controller::get_instance()->input->post("direccion"), CI_Controller::get_instance()->input->post("telefono"), CI_Controller::get_instance()->input->post("rut"), CI_Controller::get_instance()->input->post("dv")
        );
        /*
          $datos=array(
          "nombre"=>  $this->input->post("nombre"),
          "apellido" => $this->input->post("apellido"),
          "fechaNac" => $this->input->post("fechaNac"),
          "sexo" => $this->input->post("sexo"),
          "direccion" => $this->input->post("direccion"),
          "telefono" => $this->input->post("telefono"),
          "rut" => $this->input->post("rut"),
          "dv" => $this->input->post("dv")
          );
         * 
         */
        //creando reglas para ingreso de socio
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required|max_length[35]');
        $this->form_validation->set_rules('apellido', 'Apellido', 'trim|required|max_length[35]');
        $this->form_validation->set_rules('fechaNac', 'Fecha de Nacimiento', 'trim|required|max_length[25]');
        $this->form_validation->set_rules('sexo', 'Sexo', 'trim|required');
        $this->form_validation->set_rules('direccion', 'Direccion', 'trim|required|max_length[35]');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required|is_unique[socio.telefono]|max_length[40]');
        $this->form_validation->set_rules('rut', 'Rut', 'trim|required|is_unique[socio.rut]|max_length[8]');
        $this->form_validation->set_rules('dv', 'DV', 'trim|required|max_length[1]');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE) {
            //actual
            $this->load->view('ingresoSocio', $datos);
        } else {
            //si todo fue exitoso
            if ($this->validarRut(CI_Controller::get_instance()->input->post("rut")."".CI_Controller::get_instance()->input->post("dv"))) {
                $res["salida"] = "Rut correcto. " . $this->socio_model->addSocio($objeto);
                $this->load->view("salida", $res);
            } else {
                $res2["salida"] = "<center>Rut incorrecto</center>";
                $this->load->view("salida", $res2);
            }
        }
    }
    /**
     * 
     * @param type $r
     * @return boolean
     */
    //valida rut
    function validarRut($r = false) {
        if ((!$r) or ( is_array($r)))
            return false; /* Hace falta el rut */

        if (!$r = preg_replace('|[^0-9kK]|i', '', $r))
            return false; /* Era código basura */

        if (!((strlen($r) == 8) or ( strlen($r) == 9)))
            return false; /* La cantidad de carácteres no es válida. */

        $v = strtoupper(substr($r, -1));
        if (!$r = substr($r, 0, -1))
            return false;

        if (!((int) $r > 0))
            return false; /* No es un valor numérico */

        $x = 2;
        $s = 0;
        for ($i = (strlen($r) - 1); $i >= 0; $i--) {
            if ($x > 7)
                $x = 2;
            $s += ($r[$i] * $x);
            $x++;
        }
        $dv = 11 - ($s % 11);
        if ($dv == 10)
            $dv = 'K';
        if ($dv == 11)
            $dv = '0';
        if ($dv == $v)
            return number_format($r, 0, '', '.') . '-' . $v; /* Formatea el RUT */
        return false;
    }
    /**
     * 
     */
    public function index() {
        
    }
    /**
     * 
     */
    public function recibirSeleccionSocio() {
        $idSeleccion = $this->input->post("seleccion");
        $this->session->set_userdata("idSeleccion", $idSeleccion);
        $query["salida"] = $this->pelicula_model->getTodoPelicula();
        $this->load->view("listaPeliculas", $query);
    }
    /**
     * 
     */
    function recibirSeleccionSocioPrestamo() {
        //obtiene seleccion de socio del formulario
        $idSeleccion = $this->input->post("seleccion");
        //guarda el id de socio en una variable de session
        $this->session->set_userdata("idSeleccionPrestamo", $idSeleccion);
        $objeto = $this->socio_model->getSocioById($idSeleccion);
        $salida["nombreSocio"] = $objeto->getNombre() . " " . $objeto->getApellido();
        $this->load->view("administracionPrestamoVideo", $salida);
    }

}
?>