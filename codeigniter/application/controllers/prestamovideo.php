<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prestamovideo
 *
 * @author Usuario
 */
class Prestamovideo extends CI_Controller {
    /**
     * 
     */
    function __construct() {
        parent::__construct();
        //cargando models
        $this->load->model("administracion/prestamoVideo_model");
        $this->load->model("administracion/carrito_model");
        $this->load->model("administracion/socio_model");
        $this->load->model("administracion/pelicula_model");
        //cargando helper
        $this->load->helper("form_helper");
        $this->load->helper("url");
        //cargando librerias
        $this->load->library("validaciones/validacion");
        $this->load->library("session");
        $this->load->library("POJO/prestamovideoPO", NULL, "prestamovideoPO");
        $this->load->library('form_validation');
    }
    /**
     * 
     */
    function busquedaRut() {
        $this->load->view("busquedaRut");
    }
    /**
     * 
     */
    function busquedaRutPrestamo() {
        $this->load->view("busquedaRutPrestamo");
    }
    /**
     * 
     */
    function recibirSocio() {

        $idCliente = $this->input->post("seleccion");
        $objeto = $this->socio_model->getSocioById($idCliente);
    }
    /**
     * 
     */
    function index() {
        
    }
    /**
     * 
     */
    function agregarDatos() {
        //guarda la fecha actual
        $fechaActual = $this->validacion->getFechaActual();
        //estado pagado, no pagado
        $estado = $this->input->post("estado");
        //obteniendo datos de las variables de sesion
        $idSocio = $this->session->userdata("idSeleccion");
        $listIDPeli = $this->session->userdata("listIDPeli");
        $precioTotal = $this->session->userdata("precioTotal");
        $listCantidad = $this->session->userdata("listCantidad");
        //creando objeto prestamovideoPO y creando el registro en tabla prestamovideo
        for ($cont = 0; $cont < sizeof($listIDPeli); $cont++) {
            $objeto = new $this->prestamovideoPO($fechaActual, $idSocio, $listIDPeli[$cont], $estado, $precioTotal);
            $this->prestamoVideo_model->addPrestamo($objeto);
        }
        //resta la cantidad con el numero de dvd disponibles (actualizando pelicula)
        for ($cont = 0; $cont < sizeof($listIDPeli); $cont++) {
            $this->pelicula_model->updatePeliculaById((int) $listCantidad[$cont], (int) $listIDPeli[$cont]);
        }
        //mueve el carrito a prestamovideo
        $this->carrito_model->borrarByIdSocio();
        //genera un resumen de videos prestados
        $out = "<center>Resumen Videos: <br>";
        $cont = 0;
        for ($cont = 0; $cont < sizeof($listIDPeli); $cont++) {
            $query = $this->pelicula_model->getPeliculaById($listIDPeli[$cont]);
            foreach ($query->result() as $value) {
                $out.=$value->titulo . "\tCantidad : " . $listCantidad[$cont] . "\tPrecio : " . $value->precio . "<br>";
                $cont++;
            }
        }
        $out = $out . "Precio Total: " . $precioTotal . "<br>";
        $out = $out . "</center>";
        $out2["salida"] = $out;
        $this->load->view("salida", $out2);
    }
    /**
     * 
     */
    function verEstado() {
        $this->load->view("verEstado");
    }
    /**
     * 
     */
    function recibirEstadoPrestamo() {
        //direcciona a la vista que muestra registros de prestamovideo dependiendo del estado
        $estado = $this->input->post("estado");
        $idSocio = $this->session->userdata("idSeleccionPrestamo");
        $query["salida"] = $this->prestamoVideo_model->getPrestamoVideoByIdSocioEstado($idSocio, $estado);
        $this->load->view("mostrarPrestamoVideo", $query);
    }
    /**
     * 
     */
    function adminPres() {
        //muestra pendientes
        $idSocio = $this->session->userdata("idSeleccionPrestamo");
        $estado = "Pendiente";
        $query["salida"] = $this->prestamoVideo_model->getPrestamoVideoByIdSocioEstado($idSocio, $estado);
        $this->load->view("mostrarPendientes", $query);
    }
    /**
     * actualiza el estado (pagado o pendiente)
     */
    function actualizarEstado() {
        $listIdPrestamo = $this->input->post("listIdPrestamo");
        $nuevoEstadoAux = $this->input->post("estadoNuevo");
        $out = "";
        for ($cont = 0; $cont < sizeof($listIdPrestamo); $cont++) {
            $out.=$this->prestamoVideo_model->updateEstado($listIdPrestamo[$cont], $nuevoEstadoAux[$cont]) . "<br>";
        }
        $out2["salida"] = $out;
        //$out["salida"]=$nuevoEstadoAux;
        $this->load->view("salida", $out2);
    }

}
